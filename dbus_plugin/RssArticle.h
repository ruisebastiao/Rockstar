/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __RssArticle_H__
#define __RssArticle_H__

class RssArticleProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    RssArticleProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "pub.geekcentral.Rockstar.RssArticle", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create RssArticle remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant title READ __get_title__ )
    QDBusVariant __get_title__() { return QDBusVariant(fetchProperty("title")); }
    

    Q_PROPERTY(QDBusVariant description READ __get_description__ )
    QDBusVariant __get_description__() { return QDBusVariant(fetchProperty("description")); }
    

    Q_PROPERTY(QDBusVariant image READ __get_image__ )
    QDBusVariant __get_image__() { return QDBusVariant(fetchProperty("image")); }
    

    Q_PROPERTY(QDBusVariant link READ __get_link__ )
    QDBusVariant __get_link__() { return QDBusVariant(fetchProperty("link")); }
    

    Q_PROPERTY(QDBusVariant pubdate READ __get_pubdate__ )
    QDBusVariant __get_pubdate__() { return QDBusVariant(fetchProperty("pubdate")); }
    


Q_SIGNALS:
};

class RssArticle : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "pub.geekcentral.Rockstar.RssArticle")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "title") {
			    Q_EMIT __titleChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "description") {
			    Q_EMIT __descriptionChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "image") {
			    Q_EMIT __imageChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "link") {
			    Q_EMIT __linkChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "pubdate") {
			    Q_EMIT __pubdateChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new RssArticleProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    RssArticle(QObject *parent=0) : QObject(parent), m_ifc(new RssArticleProxyer("/pub/geekcentral/Rockstar/RssArticle", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant title READ __get_title__  NOTIFY __titleChanged__)
    Q_PROPERTY(QVariant description READ __get_description__  NOTIFY __descriptionChanged__)
    Q_PROPERTY(QVariant image READ __get_image__  NOTIFY __imageChanged__)
    Q_PROPERTY(QVariant link READ __get_link__  NOTIFY __linkChanged__)
    Q_PROPERTY(QVariant pubdate READ __get_pubdate__  NOTIFY __pubdateChanged__)

    //Property read methods
    const QVariant __get_title__() { return unmarsh(m_ifc->__get_title__().variant()); }
    const QVariant __get_description__() { return unmarsh(m_ifc->__get_description__().variant()); }
    const QVariant __get_image__() { return unmarsh(m_ifc->__get_image__().variant()); }
    const QVariant __get_link__() { return unmarsh(m_ifc->__get_link__().variant()); }
    const QVariant __get_pubdate__() { return unmarsh(m_ifc->__get_pubdate__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:

Q_SIGNALS:
//Property changed notify signal
    void __titleChanged__(QVariant);
    void __descriptionChanged__(QVariant);
    void __imageChanged__(QVariant);
    void __linkChanged__(QVariant);
    void __pubdateChanged__(QVariant);

//DBus Interface's signal
private:
    RssArticleProxyer *m_ifc;
};

#endif

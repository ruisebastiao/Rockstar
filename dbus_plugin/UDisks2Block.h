/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __Block_H__
#define __Block_H__

class BlockProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    BlockProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.freedesktop.UDisks2.Block", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create Block remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant Device READ __get_Device__ )
    QDBusVariant __get_Device__() { return QDBusVariant(fetchProperty("Device")); }
    

    Q_PROPERTY(QDBusVariant PreferredDevice READ __get_PreferredDevice__ )
    QDBusVariant __get_PreferredDevice__() { return QDBusVariant(fetchProperty("PreferredDevice")); }
    

    Q_PROPERTY(QDBusVariant Symlinks READ __get_Symlinks__ )
    QDBusVariant __get_Symlinks__() { return QDBusVariant(fetchProperty("Symlinks")); }
    

    Q_PROPERTY(QDBusVariant DeviceNumber READ __get_DeviceNumber__ )
    QDBusVariant __get_DeviceNumber__() { return QDBusVariant(fetchProperty("DeviceNumber")); }
    

    Q_PROPERTY(QDBusVariant Id READ __get_Id__ )
    QDBusVariant __get_Id__() { return QDBusVariant(fetchProperty("Id")); }
    

    Q_PROPERTY(QDBusVariant Size READ __get_Size__ )
    QDBusVariant __get_Size__() { return QDBusVariant(fetchProperty("Size")); }
    

    Q_PROPERTY(QDBusVariant ReadOnly READ __get_ReadOnly__ )
    QDBusVariant __get_ReadOnly__() { return QDBusVariant(fetchProperty("ReadOnly")); }
    

    Q_PROPERTY(QDBusVariant Drive READ __get_Drive__ )
    QDBusVariant __get_Drive__() { return QDBusVariant(fetchProperty("Drive")); }
    

    Q_PROPERTY(QDBusVariant MDRaid READ __get_MDRaid__ )
    QDBusVariant __get_MDRaid__() { return QDBusVariant(fetchProperty("MDRaid")); }
    

    Q_PROPERTY(QDBusVariant MDRaidMember READ __get_MDRaidMember__ )
    QDBusVariant __get_MDRaidMember__() { return QDBusVariant(fetchProperty("MDRaidMember")); }
    

    Q_PROPERTY(QDBusVariant IdUsage READ __get_IdUsage__ )
    QDBusVariant __get_IdUsage__() { return QDBusVariant(fetchProperty("IdUsage")); }
    

    Q_PROPERTY(QDBusVariant IdType READ __get_IdType__ )
    QDBusVariant __get_IdType__() { return QDBusVariant(fetchProperty("IdType")); }
    

    Q_PROPERTY(QDBusVariant IdVersion READ __get_IdVersion__ )
    QDBusVariant __get_IdVersion__() { return QDBusVariant(fetchProperty("IdVersion")); }
    

    Q_PROPERTY(QDBusVariant IdLabel READ __get_IdLabel__ )
    QDBusVariant __get_IdLabel__() { return QDBusVariant(fetchProperty("IdLabel")); }
    

    Q_PROPERTY(QDBusVariant IdUUID READ __get_IdUUID__ )
    QDBusVariant __get_IdUUID__() { return QDBusVariant(fetchProperty("IdUUID")); }
    

    Q_PROPERTY(QDBusVariant Configuration READ __get_Configuration__ )
    QDBusVariant __get_Configuration__() { return QDBusVariant(fetchProperty("Configuration")); }
    

    Q_PROPERTY(QDBusVariant CryptoBackingDevice READ __get_CryptoBackingDevice__ )
    QDBusVariant __get_CryptoBackingDevice__() { return QDBusVariant(fetchProperty("CryptoBackingDevice")); }
    

    Q_PROPERTY(QDBusVariant HintPartitionable READ __get_HintPartitionable__ )
    QDBusVariant __get_HintPartitionable__() { return QDBusVariant(fetchProperty("HintPartitionable")); }
    

    Q_PROPERTY(QDBusVariant HintSystem READ __get_HintSystem__ )
    QDBusVariant __get_HintSystem__() { return QDBusVariant(fetchProperty("HintSystem")); }
    

    Q_PROPERTY(QDBusVariant HintIgnore READ __get_HintIgnore__ )
    QDBusVariant __get_HintIgnore__() { return QDBusVariant(fetchProperty("HintIgnore")); }
    

    Q_PROPERTY(QDBusVariant HintAuto READ __get_HintAuto__ )
    QDBusVariant __get_HintAuto__() { return QDBusVariant(fetchProperty("HintAuto")); }
    

    Q_PROPERTY(QDBusVariant HintName READ __get_HintName__ )
    QDBusVariant __get_HintName__() { return QDBusVariant(fetchProperty("HintName")); }
    

    Q_PROPERTY(QDBusVariant HintIconName READ __get_HintIconName__ )
    QDBusVariant __get_HintIconName__() { return QDBusVariant(fetchProperty("HintIconName")); }
    

    Q_PROPERTY(QDBusVariant HintSymbolicIconName READ __get_HintSymbolicIconName__ )
    QDBusVariant __get_HintSymbolicIconName__() { return QDBusVariant(fetchProperty("HintSymbolicIconName")); }
    


Q_SIGNALS:
};

class Block : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.freedesktop.UDisks2.Block")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "Device") {
			    Q_EMIT __deviceChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "PreferredDevice") {
			    Q_EMIT __preferredDeviceChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Symlinks") {
			    Q_EMIT __symlinksChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "DeviceNumber") {
			    Q_EMIT __deviceNumberChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Id") {
			    Q_EMIT __idChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Size") {
			    Q_EMIT __sizeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "ReadOnly") {
			    Q_EMIT __readOnlyChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Drive") {
			    Q_EMIT __driveChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MDRaid") {
			    Q_EMIT __mDRaidChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "MDRaidMember") {
			    Q_EMIT __mDRaidMemberChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IdUsage") {
			    Q_EMIT __idUsageChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IdType") {
			    Q_EMIT __idTypeChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IdVersion") {
			    Q_EMIT __idVersionChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IdLabel") {
			    Q_EMIT __idLabelChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "IdUUID") {
			    Q_EMIT __idUUIDChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Configuration") {
			    Q_EMIT __configurationChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CryptoBackingDevice") {
			    Q_EMIT __cryptoBackingDeviceChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintPartitionable") {
			    Q_EMIT __hintPartitionableChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintSystem") {
			    Q_EMIT __hintSystemChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintIgnore") {
			    Q_EMIT __hintIgnoreChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintAuto") {
			    Q_EMIT __hintAutoChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintName") {
			    Q_EMIT __hintNameChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintIconName") {
			    Q_EMIT __hintIconNameChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HintSymbolicIconName") {
			    Q_EMIT __hintSymbolicIconNameChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new BlockProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    Block(QObject *parent=0) : QObject(parent), m_ifc(new BlockProxyer("/org/freedesktop/UDisks2/Block", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant device READ __get_Device__  NOTIFY __deviceChanged__)
    Q_PROPERTY(QVariant preferredDevice READ __get_PreferredDevice__  NOTIFY __preferredDeviceChanged__)
    Q_PROPERTY(QVariant symlinks READ __get_Symlinks__  NOTIFY __symlinksChanged__)
    Q_PROPERTY(QVariant deviceNumber READ __get_DeviceNumber__  NOTIFY __deviceNumberChanged__)
    Q_PROPERTY(QVariant id READ __get_Id__  NOTIFY __idChanged__)
    Q_PROPERTY(QVariant size READ __get_Size__  NOTIFY __sizeChanged__)
    Q_PROPERTY(QVariant readOnly READ __get_ReadOnly__  NOTIFY __readOnlyChanged__)
    Q_PROPERTY(QVariant drive READ __get_Drive__  NOTIFY __driveChanged__)
    Q_PROPERTY(QVariant mDRaid READ __get_MDRaid__  NOTIFY __mDRaidChanged__)
    Q_PROPERTY(QVariant mDRaidMember READ __get_MDRaidMember__  NOTIFY __mDRaidMemberChanged__)
    Q_PROPERTY(QVariant idUsage READ __get_IdUsage__  NOTIFY __idUsageChanged__)
    Q_PROPERTY(QVariant idType READ __get_IdType__  NOTIFY __idTypeChanged__)
    Q_PROPERTY(QVariant idVersion READ __get_IdVersion__  NOTIFY __idVersionChanged__)
    Q_PROPERTY(QVariant idLabel READ __get_IdLabel__  NOTIFY __idLabelChanged__)
    Q_PROPERTY(QVariant idUUID READ __get_IdUUID__  NOTIFY __idUUIDChanged__)
    Q_PROPERTY(QVariant configuration READ __get_Configuration__  NOTIFY __configurationChanged__)
    Q_PROPERTY(QVariant cryptoBackingDevice READ __get_CryptoBackingDevice__  NOTIFY __cryptoBackingDeviceChanged__)
    Q_PROPERTY(QVariant hintPartitionable READ __get_HintPartitionable__  NOTIFY __hintPartitionableChanged__)
    Q_PROPERTY(QVariant hintSystem READ __get_HintSystem__  NOTIFY __hintSystemChanged__)
    Q_PROPERTY(QVariant hintIgnore READ __get_HintIgnore__  NOTIFY __hintIgnoreChanged__)
    Q_PROPERTY(QVariant hintAuto READ __get_HintAuto__  NOTIFY __hintAutoChanged__)
    Q_PROPERTY(QVariant hintName READ __get_HintName__  NOTIFY __hintNameChanged__)
    Q_PROPERTY(QVariant hintIconName READ __get_HintIconName__  NOTIFY __hintIconNameChanged__)
    Q_PROPERTY(QVariant hintSymbolicIconName READ __get_HintSymbolicIconName__  NOTIFY __hintSymbolicIconNameChanged__)

    //Property read methods
    const QVariant __get_Device__() { return unmarsh(m_ifc->__get_Device__().variant()); }
    const QVariant __get_PreferredDevice__() { return unmarsh(m_ifc->__get_PreferredDevice__().variant()); }
    const QVariant __get_Symlinks__() { return unmarsh(m_ifc->__get_Symlinks__().variant()); }
    const QVariant __get_DeviceNumber__() { return unmarsh(m_ifc->__get_DeviceNumber__().variant()); }
    const QVariant __get_Id__() { return unmarsh(m_ifc->__get_Id__().variant()); }
    const QVariant __get_Size__() { return unmarsh(m_ifc->__get_Size__().variant()); }
    const QVariant __get_ReadOnly__() { return unmarsh(m_ifc->__get_ReadOnly__().variant()); }
    const QVariant __get_Drive__() { return unmarsh(m_ifc->__get_Drive__().variant()); }
    const QVariant __get_MDRaid__() { return unmarsh(m_ifc->__get_MDRaid__().variant()); }
    const QVariant __get_MDRaidMember__() { return unmarsh(m_ifc->__get_MDRaidMember__().variant()); }
    const QVariant __get_IdUsage__() { return unmarsh(m_ifc->__get_IdUsage__().variant()); }
    const QVariant __get_IdType__() { return unmarsh(m_ifc->__get_IdType__().variant()); }
    const QVariant __get_IdVersion__() { return unmarsh(m_ifc->__get_IdVersion__().variant()); }
    const QVariant __get_IdLabel__() { return unmarsh(m_ifc->__get_IdLabel__().variant()); }
    const QVariant __get_IdUUID__() { return unmarsh(m_ifc->__get_IdUUID__().variant()); }
    const QVariant __get_Configuration__() { return unmarsh(m_ifc->__get_Configuration__().variant()); }
    const QVariant __get_CryptoBackingDevice__() { return unmarsh(m_ifc->__get_CryptoBackingDevice__().variant()); }
    const QVariant __get_HintPartitionable__() { return unmarsh(m_ifc->__get_HintPartitionable__().variant()); }
    const QVariant __get_HintSystem__() { return unmarsh(m_ifc->__get_HintSystem__().variant()); }
    const QVariant __get_HintIgnore__() { return unmarsh(m_ifc->__get_HintIgnore__().variant()); }
    const QVariant __get_HintAuto__() { return unmarsh(m_ifc->__get_HintAuto__().variant()); }
    const QVariant __get_HintName__() { return unmarsh(m_ifc->__get_HintName__().variant()); }
    const QVariant __get_HintIconName__() { return unmarsh(m_ifc->__get_HintIconName__().variant()); }
    const QVariant __get_HintSymbolicIconName__() { return unmarsh(m_ifc->__get_HintSymbolicIconName__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant AddConfigurationItem(const QVariant &item, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), item, "(sa{sv})") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("AddConfigurationItem"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.AddConfigurationItem:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant RemoveConfigurationItem(const QVariant &item, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), item, "(sa{sv})") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("RemoveConfigurationItem"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.RemoveConfigurationItem:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant UpdateConfigurationItem(const QVariant &old_item, const QVariant &new_item, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), old_item, "(sa{sv})") << marsh(QDBusArgument(), new_item, "(sa{sv})") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("UpdateConfigurationItem"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.UpdateConfigurationItem:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant GetSecretConfiguration(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("GetSecretConfiguration"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.GetSecretConfiguration:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UDisks2.Block.GetSecretConfiguration\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant Format(const QVariant &type_, const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), type_, "s") << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Format"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.Format:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant OpenForBackup(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("OpenForBackup"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.OpenForBackup:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UDisks2.Block.OpenForBackup\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant OpenForRestore(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("OpenForRestore"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.OpenForRestore:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UDisks2.Block.OpenForRestore\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant OpenForBenchmark(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("OpenForBenchmark"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.OpenForBenchmark:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    if (args.size() != 1) {
		    qDebug() << "Warning: \"org.freedesktop.UDisks2.Block.OpenForBenchmark\" excepted one output parameter, but got " << args.size();
		    return QVariant();
	    }
	    return unmarsh(args[0]);
	    
    }
  
    QVariant Rescan(const QVariant &options) {
	    QList<QVariant> argumentList;
	    argumentList << marsh(QDBusArgument(), options, "a{sv}");

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Rescan"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.freedesktop.UDisks2.Block.Rescan:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __deviceChanged__(QVariant);
    void __preferredDeviceChanged__(QVariant);
    void __symlinksChanged__(QVariant);
    void __deviceNumberChanged__(QVariant);
    void __idChanged__(QVariant);
    void __sizeChanged__(QVariant);
    void __readOnlyChanged__(QVariant);
    void __driveChanged__(QVariant);
    void __mDRaidChanged__(QVariant);
    void __mDRaidMemberChanged__(QVariant);
    void __idUsageChanged__(QVariant);
    void __idTypeChanged__(QVariant);
    void __idVersionChanged__(QVariant);
    void __idLabelChanged__(QVariant);
    void __idUUIDChanged__(QVariant);
    void __configurationChanged__(QVariant);
    void __cryptoBackingDeviceChanged__(QVariant);
    void __hintPartitionableChanged__(QVariant);
    void __hintSystemChanged__(QVariant);
    void __hintIgnoreChanged__(QVariant);
    void __hintAutoChanged__(QVariant);
    void __hintNameChanged__(QVariant);
    void __hintIconNameChanged__(QVariant);
    void __hintSymbolicIconNameChanged__(QVariant);

//DBus Interface's signal
private:
    BlockProxyer *m_ifc;
};

#endif

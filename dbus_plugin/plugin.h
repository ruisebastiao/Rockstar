
#ifndef __DBUS_H__
#define __DBUS_H__


#include "Alexa.h"
#include "Mpris.h"
#include "ObjectManager.h"
#include "Player.h"
#include "Playlists.h"
#include "Rss.h"
#include "RssArticle.h"
#include "Settings.h"
#include "UDisks2Block.h"
#include "UDisks2Drive.h"
#include "UDisks2Filesystem.h"
#include "UDisks2Manager.h"
#include "UPower.h"
#include "UPowerDevice.h"
#include "Weather.h"
#include <QQmlExtensionPlugin>
#include <qqml.h>

class DBusPlugin: public QQmlExtensionPlugin
{
    Q_OBJECT
        Q_PLUGIN_METADATA(IID "com.deepin.dde.daemon.DBus")

    public:
        void registerTypes(const char* uri) { 
             qmlRegisterType<Alexa>(uri, 1, 0, "Alexa");
             qmlRegisterType<Settings>(uri, 1, 0, "Settings");
             qmlRegisterType<Weather>(uri, 1, 0, "Weather");
             qmlRegisterType<Rss>(uri, 1, 0, "Rss");
             qmlRegisterType<RssArticle>(uri, 1, 0, "RssArticle");
             qmlRegisterType<MPRIS>(uri, 1, 0, "MPRIS");
             qmlRegisterType<Player>(uri, 1, 0, "Player");
             qmlRegisterType<Playlists>(uri, 1, 0, "Playlists");
             qmlRegisterType<UPower>(uri, 1, 0, "UPower");
             qmlRegisterType<Device>(uri, 1, 0, "Device");
             qmlRegisterType<ObjectManager>(uri, 1, 0, "ObjectManager");
             qmlRegisterType<Manager>(uri, 1, 0, "Manager");
             qmlRegisterType<Drive>(uri, 1, 0, "Drive");
             qmlRegisterType<Block>(uri, 1, 0, "Block");
             qmlRegisterType<Filesystem>(uri, 1, 0, "Filesystem");
        }
};

#include <libintl.h>
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v)
{
	if (v.type() != QMetaType::QString) {
		return v;
	}
	bindtextdomain(domain, dir);
	return QVariant::fromValue<QString>(dgettext(domain, v.toString().toLocal8Bit()));
}

inline
int getTypeId(const QString& sig) {
    //TODO: this should staticly generate by xml info
    if (0) { 
    } else if (sig == "(b(oss))") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "(sa{sv})") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "a(dd)") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "a(oss)") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "a(sa{sv})") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "a(udu)") {
	    return qDBusRegisterMetaType<QVariantList >();
    } else if (sig == "aay") {
	    return qDBusRegisterMetaType<QList< QList< uchar > > >();
    } else if (sig == "ao") {
	    return qDBusRegisterMetaType<QList< QDBusObjectPath > >();
    } else if (sig == "as") {
	    return qDBusRegisterMetaType<QStringList >();
    } else if (sig == "ay") {
	    return qDBusRegisterMetaType<QList< uchar > >();
    } else if (sig == "a{oa{sa{sv}}}") {
	    return qDBusRegisterMetaType<QVariantMap >();
    } else if (sig == "a{sa{sv}}") {
	    return qDBusRegisterMetaType<QVariantMap >();
    } else if (sig == "a{sv}") {
	    return qDBusRegisterMetaType<QVariantMap >();
    } else if (sig == "b") {
	    return qDBusRegisterMetaType<bool >();
    } else if (sig == "d") {
	    return qDBusRegisterMetaType<double >();
    } else if (sig == "h") {
	    return qDBusRegisterMetaType<uint >();
    } else if (sig == "i") {
	    return qDBusRegisterMetaType<int >();
    } else if (sig == "o") {
	    return qDBusRegisterMetaType<QDBusObjectPath >();
    } else if (sig == "s") {
	    return qDBusRegisterMetaType<QString >();
    } else if (sig == "t") {
	    return qDBusRegisterMetaType<qulonglong >();
    } else if (sig == "u") {
	    return qDBusRegisterMetaType<uint >();
    } else if (sig == "v") {
	    return qDBusRegisterMetaType<QDBusSignature >();
    } else if (sig == "x") {
	    return qDBusRegisterMetaType<qlonglong >();
    } else if (sig == "y") {
	    return qDBusRegisterMetaType<uchar >();
    } else if (sig == "(iiii)") {
	    return qDBusRegisterMetaType<QRect>();
    } else {
	    qDebug() << "Didn't support getTypeId" << sig << " please report it to snyh@snyh.org";
    }
}

inline
QVariant qstring2dbus(QString value, char sig) {
    switch (sig) {
        case 'y':
            return QVariant::fromValue(uchar(value[0].toLatin1()));
        case 'n':
            return QVariant::fromValue(value.toShort());
        case 'q':
            return QVariant::fromValue(value.toUShort());
        case 'i':
            return QVariant::fromValue(value.toInt());
        case 'u':
            return QVariant::fromValue(value.toUInt());
        case 'x':
            return QVariant::fromValue(value.toLongLong());
        case 't':
            return QVariant::fromValue(value.toULongLong());
        case 'd':
            return QVariant::fromValue(value.toDouble());
        case 's':
            return QVariant::fromValue(value);
        case 'o':
            return QVariant::fromValue(QDBusObjectPath(value));
        case 'v':
            return QVariant::fromValue(QDBusSignature(value));
        default:
            qDebug() << "Dict entry key should be an basic dbus type not an " << sig;
            return QVariant();
    }
}

QList<QString> splitStructureSignature(const QString& sig) {
    if (sig.size() < 3 || sig[0] != '(' || sig[sig.size()-1] != ')') {
        return QList<QString>();
    }

    QList<QString> sigs;

    QString tmp = sig.mid(1, sig.size()-2);
    while (tmp.size() != 0) {
        switch (tmp[0].toLatin1()) {
            case 'a':
                if (tmp.size() < 2) {
                    return QList<QString>();
                }
                if (tmp[1] == '{') {
                    int lastIndex = tmp.lastIndexOf('}') + 1;
                    if (lastIndex == 0) return QList<QString>();
                    sigs.append(tmp.mid(0, lastIndex));
                    tmp = tmp.mid(lastIndex);
                    break;
                } else if (tmp[1] == '(') {
                    int lastIndex = tmp.lastIndexOf(')') + 1;
                    if (lastIndex == 0) return QList<QString>();
                    sigs.append(tmp.mid(0, lastIndex));
                    tmp = tmp.mid(lastIndex);
                    break;
                } else {
                    sigs.append(tmp.mid(0, 2));
                    tmp = tmp.mid(2);
                    break;
                }
            case '(': {
                          int lastIndex = tmp.lastIndexOf(')') + 1;
                          if (lastIndex == 0) return QList<QString>();
                          sigs.append(tmp.mid(0, lastIndex));
                          tmp = tmp.mid(lastIndex);
                          break;
                      }
            case 'y': case 'b': case 'n': case 'q':
            case 'i': case 'u': case 'x': case 't':
            case 'd': case 's': case 'o': case 'g':
            case 'h': case 'v':
                sigs.append(QString(tmp[0]));
                tmp = tmp.mid(1, tmp.size() - 1);
                break;
            default:
                return QList<QString>();
        }
    }
    return sigs;
}

QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig) {
    if (sig.size() == 0) {
        return QVariant::fromValue(target);
    }
    switch (sig[0].toLatin1()) {
        case 'y':
            target << qstring2dbus(arg.value<QString>(), 'y').value<uchar>();
            return QVariant::fromValue(target);
        case 'b':
            target << arg.value<bool>();
            return QVariant::fromValue(target);
        case 'n':
            target << arg.value<short>();
            return QVariant::fromValue(target);
        case 'q':
            target << arg.value<ushort>();
            return QVariant::fromValue(target);
        case 'i':
            target << arg.value<qint32>();
            return QVariant::fromValue(target);
        case 'u':
            target << arg.value<quint32>();
            return QVariant::fromValue(target);
        case 'x':
            target << arg.value<qlonglong>();
            return QVariant::fromValue(target);
        case 't':
            target << arg.value<qulonglong>();
            return QVariant::fromValue(target);
        case 'd':
            target << arg.value<double>();
            return QVariant::fromValue(target);
        case 's':
            target << arg.value<QString>();
            return QVariant::fromValue(target);
        case 'o':
            target << QDBusObjectPath(arg.value<QString>());
            return QVariant::fromValue(target);
        case 'g':
            target << QDBusSignature(arg.value<QString>());
            return QVariant::fromValue(target);

        case 'a':
            {
                if (sig.size() < 2) { return QVariant(); }
                char s = sig[1].toLatin1();
                if (s == '{') {
                    char key_sig = sig[2].toLatin1();
                    QString value_sig = sig.mid(3, sig.lastIndexOf('}') - 3);
                    target.beginMap(getTypeId(QString(key_sig)), getTypeId(value_sig));
                    //qDebug() << "BeginMap:" << key_sig << value_sig;
                    foreach(const QString& key, arg.value<QVariantMap>().keys()) {
                        //qDebug() << "KEY:" << key;
                        target.beginMapEntry();
                        //qDebug() <<"beginMapEntry";
                        marsh(target, qstring2dbus(key, key_sig), QString(key_sig));
                        marsh(target, arg.value<QVariantMap>()[key], value_sig);
                        //qDebug() <<"EndMapEntry";
                        target.endMapEntry();
                    }
                    //qDebug() << "EndMap";
                    target.endMap();
                    return QVariant::fromValue(target);
                } else {
                    QString next = sig.right(sig.size() - 1);
                    target.beginArray(getTypeId(next));
                    foreach(const QVariant& v, arg.value<QVariantList>()) {
                        marsh(target, v, next);
                    }
                    target.endArray();
                    return QVariant::fromValue(target);
                }
            }
        case '(':
            {
                QList<QString> sigs = splitStructureSignature(sig);
                QVariantList values = arg.value<QVariantList>();
                if (values.size() != sigs.size()) {
                    qDebug() << "structure (" << arg << ") didn't match signature :" << sigs;
                    return QVariant();
                }
                target.beginStructure();
                for (int i=0; i < sigs.size(); i++) {
                    marsh(target, values[i], sigs[i]);
                }
                target.endStructure();
                return QVariant::fromValue(target);
            }
        default:
            qDebug() << "Panic didn't support marsh" << sig;
    }
    return QVariant::fromValue(target);
}

inline
QVariant unmarshDBus(const QDBusArgument &argument)
{
    switch (argument.currentType()) {
    case QDBusArgument::BasicType: {
        QVariant v = argument.asVariant();
        if (v.userType() == qMetaTypeId<QDBusObjectPath>())
            return v.value<QDBusObjectPath>().path();
        else if (v.userType() == qMetaTypeId<QDBusSignature>())
            return v.value<QDBusSignature>().signature();
        else
            return v;
    }
    case QDBusArgument::VariantType: {
        QVariant v = argument.asVariant().value<QDBusVariant>().variant();
        if (v.userType() == qMetaTypeId<QDBusArgument>())
            return unmarshDBus(v.value<QDBusArgument>());
        else
            return v;
    }
    case QDBusArgument::ArrayType: {
        QVariantList list;
        argument.beginArray();
        while (!argument.atEnd())
            list.append(unmarshDBus(argument));
        argument.endArray();
        return list;
    }
    case QDBusArgument::StructureType: {
        QVariantList list;
        argument.beginStructure();
        while (!argument.atEnd())
            list.append(unmarshDBus(argument));
        argument.endStructure();
        return QVariant::fromValue(list);
    }
    case QDBusArgument::MapType: {
        QVariantMap map;
        argument.beginMap();
        while (!argument.atEnd()) {
            argument.beginMapEntry();
            QVariant key = unmarshDBus(argument);
            QVariant value = unmarshDBus(argument);
            map.insert(key.toString(), value);
            argument.endMapEntry();
        }
        argument.endMap();
        return map;
    }
    default:
        return QVariant();
        break;
    }
}

QVariant unmarsh(const QVariant& v) {
    if (v.userType() == qMetaTypeId<QDBusObjectPath>()) {
        return QVariant::fromValue(v.value<QDBusObjectPath>().path());
    } else if (v.userType() == qMetaTypeId<QDBusArgument>()) {
        return unmarsh(unmarshDBus(v.value<QDBusArgument>()));
    } else if (v.userType() == qMetaTypeId<QByteArray>()) {
        return QString(v.value<QByteArray>());
    }
    return v;
}

#endif

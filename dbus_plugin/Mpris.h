/*This file is auto generate by pkg.deepin.io/dbus-generator. Don't edit it*/
#include <QtDBus>
QVariant unmarsh(const QVariant&);
QVariant marsh(QDBusArgument target, const QVariant& arg, const QString& sig);
QVariant translateI18n(const char* dir, const char* domain,  const QVariant v);

#ifndef __MPRIS_H__
#define __MPRIS_H__

class MPRISProxyer: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    MPRISProxyer(const QString &path, QObject* parent)
          :QDBusAbstractInterface("pub.geekcentral.Rockstar", path, "org.mpris.MediaPlayer2", QDBusConnection::sessionBus(), parent)
    {
	    if (!isValid()) {
		    qDebug() << "Create MPRIS remote object failed : " << lastError().message();
	    }
    }
    QVariant fetchProperty(const char* name) {
	QDBusMessage msg = QDBusMessage::createMethodCall(service(), path(),
		QLatin1String("org.freedesktop.DBus.Properties"),
		QLatin1String("Get"));
	msg << interface() << QString::fromUtf8(name);
	QDBusMessage reply = connection().call(msg, QDBus::Block, timeout());
	if (reply.type() != QDBusMessage::ReplyMessage) {
	    qDebug () << QDBusError(reply) << "at " << service() << path() << interface() << name;
	    return QVariant();
	}
	if (reply.signature() != QLatin1String("v")) {
	    QString errmsg = QLatin1String("Invalid signature org.freedesktop.DBus.Propertyies in return from call to ");
	    qDebug () << QDBusError(QDBusError::InvalidSignature, errmsg.arg(reply.signature()));
	    return QVariant();
	}

	QVariant value = qvariant_cast<QDBusVariant>(reply.arguments().at(0)).variant();
	return value;
    }


    Q_PROPERTY(QDBusVariant CanQuit READ __get_CanQuit__ )
    QDBusVariant __get_CanQuit__() { return QDBusVariant(fetchProperty("CanQuit")); }
    

    Q_PROPERTY(QDBusVariant CanRaise READ __get_CanRaise__ )
    QDBusVariant __get_CanRaise__() { return QDBusVariant(fetchProperty("CanRaise")); }
    

    Q_PROPERTY(QDBusVariant HasTrackList READ __get_HasTrackList__ )
    QDBusVariant __get_HasTrackList__() { return QDBusVariant(fetchProperty("HasTrackList")); }
    

    Q_PROPERTY(QDBusVariant Identity READ __get_Identity__ )
    QDBusVariant __get_Identity__() { return QDBusVariant(fetchProperty("Identity")); }
    

    Q_PROPERTY(QDBusVariant DesktopEntry READ __get_DesktopEntry__ )
    QDBusVariant __get_DesktopEntry__() { return QDBusVariant(fetchProperty("DesktopEntry")); }
    

    Q_PROPERTY(QDBusVariant SupportedUriSchemes READ __get_SupportedUriSchemes__ )
    QDBusVariant __get_SupportedUriSchemes__() { return QDBusVariant(fetchProperty("SupportedUriSchemes")); }
    

    Q_PROPERTY(QDBusVariant SupportedMimeTypes READ __get_SupportedMimeTypes__ )
    QDBusVariant __get_SupportedMimeTypes__() { return QDBusVariant(fetchProperty("SupportedMimeTypes")); }
    


Q_SIGNALS:
};

class MPRIS : public QObject 
{
    Q_OBJECT
private:
    QString m_path;
    Q_SLOT void _propertiesChanged(const QDBusMessage &msg) {
	    QList<QVariant> arguments = msg.arguments();
	    if (3 != arguments.count())
	    	return;
	    QString interfaceName = msg.arguments().at(0).toString();
	    if (interfaceName != "org.mpris.MediaPlayer2")
		    return;

	    QVariantMap changedProps = qdbus_cast<QVariantMap>(arguments.at(1).value<QDBusArgument>());
	    foreach(const QString &prop, changedProps.keys()) {
		    if (0) { 
		    } else if (prop == "CanQuit") {
			    Q_EMIT __canQuitChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "CanRaise") {
			    Q_EMIT __canRaiseChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "HasTrackList") {
			    Q_EMIT __hasTrackListChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "Identity") {
			    Q_EMIT __identityChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "DesktopEntry") {
			    Q_EMIT __desktopEntryChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "SupportedUriSchemes") {
			    Q_EMIT __supportedUriSchemesChanged__(unmarsh(changedProps.value(prop)));
		    } else if (prop == "SupportedMimeTypes") {
			    Q_EMIT __supportedMimeTypesChanged__(unmarsh(changedProps.value(prop)));
		    }
	    }
    }
    void _rebuild() 
    { 
	  delete m_ifc;
          m_ifc = new MPRISProxyer(m_path, this);
	  _setupSignalHandle();
    }
    void _setupSignalHandle() {

    }
public:
    Q_PROPERTY(QString path READ path WRITE setPath NOTIFY pathChanged)
    const QString path() {
	    return m_path;
    }
    void setPath(const QString& path) {
	    QDBusConnection::sessionBus().disconnect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				 this, SLOT(_propertiesChanged(QDBusMessage)));
	    m_path = path;
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
	    _rebuild();
    }
    Q_SIGNAL void pathChanged(QString);

    MPRIS(QObject *parent=0) : QObject(parent), m_ifc(new MPRISProxyer("/org/mpris/MediaPlayer2", this))
    {
	    _setupSignalHandle();
	    QDBusConnection::sessionBus().connect("pub.geekcentral.Rockstar", m_path, "org.freedesktop.DBus.Properties", "PropertiesChanged",
	    				"sa{sv}as", this, SLOT(_propertiesChanged(QDBusMessage)));
    }
    
    Q_PROPERTY(QVariant canQuit READ __get_CanQuit__  NOTIFY __canQuitChanged__)
    Q_PROPERTY(QVariant canRaise READ __get_CanRaise__  NOTIFY __canRaiseChanged__)
    Q_PROPERTY(QVariant hasTrackList READ __get_HasTrackList__  NOTIFY __hasTrackListChanged__)
    Q_PROPERTY(QVariant identity READ __get_Identity__  NOTIFY __identityChanged__)
    Q_PROPERTY(QVariant desktopEntry READ __get_DesktopEntry__  NOTIFY __desktopEntryChanged__)
    Q_PROPERTY(QVariant supportedUriSchemes READ __get_SupportedUriSchemes__  NOTIFY __supportedUriSchemesChanged__)
    Q_PROPERTY(QVariant supportedMimeTypes READ __get_SupportedMimeTypes__  NOTIFY __supportedMimeTypesChanged__)

    //Property read methods
    const QVariant __get_CanQuit__() { return unmarsh(m_ifc->__get_CanQuit__().variant()); }
    const QVariant __get_CanRaise__() { return unmarsh(m_ifc->__get_CanRaise__().variant()); }
    const QVariant __get_HasTrackList__() { return unmarsh(m_ifc->__get_HasTrackList__().variant()); }
    const QVariant __get_Identity__() { return unmarsh(m_ifc->__get_Identity__().variant()); }
    const QVariant __get_DesktopEntry__() { return unmarsh(m_ifc->__get_DesktopEntry__().variant()); }
    const QVariant __get_SupportedUriSchemes__() { return unmarsh(m_ifc->__get_SupportedUriSchemes__().variant()); }
    const QVariant __get_SupportedMimeTypes__() { return unmarsh(m_ifc->__get_SupportedMimeTypes__().variant()); }
    //Property set methods :TODO check access

public Q_SLOTS:  
    QVariant Raise() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Raise"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Raise:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }
  
    QVariant Quit() {
	    QList<QVariant> argumentList;
	    argumentList;

	    QDBusPendingReply<> call = m_ifc->asyncCallWithArgumentList(QLatin1String("Quit"), argumentList);
	    call.waitForFinished();
	    if (!call.isValid()) {
		    qDebug() << "Error at org.mpris.MediaPlayer2.Quit:" << call.error().message();
		    return QVariant();
	    }
	    QList<QVariant> args = call.reply().arguments();

	    
	    return QVariant();
	    
    }


Q_SIGNALS:
//Property changed notify signal
    void __canQuitChanged__(QVariant);
    void __canRaiseChanged__(QVariant);
    void __hasTrackListChanged__(QVariant);
    void __identityChanged__(QVariant);
    void __desktopEntryChanged__(QVariant);
    void __supportedUriSchemesChanged__(QVariant);
    void __supportedMimeTypesChanged__(QVariant);

//DBus Interface's signal
private:
    MPRISProxyer *m_ifc;
};

#endif

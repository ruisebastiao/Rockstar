import QtQuick 2.4
import Material 0.3
import Material.Extras 0.1
import Material.ListItems 0.1

Card {

    property alias title: topSection.text
    property alias iconName: topSection.iconName
    property alias iconSource: topSection.iconSource
    default property alias data: contentCardLoader.sourceComponent

    id: summaryCard
    height: contentCardLoader.height + topSection.height
    width: Math.max(contentCardLoader.width, dp(256))

    elevation: 1

    CardHeader {
        id: topSection
        showDivider: true
    }

    View {
        id: contentCard
        anchors.top: topSection.bottom
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        Loader {
            id: contentCardLoader
        }
    }
}

import QtQuick 2.6
import Material 0.3

//http://www.openweathermap.org/api
// API Key
// City
Page {
    id: page
    title: "Weather"

    actions: [
        Action {
            name: "Search Location"
            iconSource: "icon://awesome/search"
        },
        Action {
            name: "Info"
            iconSource: "icon://awesome/info"
        },
        Action {
            name: "Options"
            iconSource: "icon://awesome/cog"
        }
    ]

    actionBar {

    }

    Component.onDestruction: console.log("going away")
}

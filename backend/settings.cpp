#include "settings.h"
#include <QDBusConnection>

Settings::Settings(QObject *parent) : QObject(parent)
{
    m_settings = new QSettings(QLatin1String("pub.geekcentral"),
                               QLatin1String("Rockstar"),
                               this);
    m_adaptor = new SettingsAdaptor(this);
    QDBusConnection::sessionBus().registerObject("/pub/geekcentral/Rockstar/Settings", this);
}

void Settings::setValue(const QString &key, const QDBusVariant &value)
{
    if ( m_settings->value(key) != value.variant() ) {
        m_settings->setValue(key,value.variant());
    }
}

QDBusVariant Settings::value(const QString &key, const QDBusVariant &defaultValue)
{
    return QDBusVariant(m_settings->value(key,defaultValue.variant()));
}

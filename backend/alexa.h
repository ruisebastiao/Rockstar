#ifndef ALEXA_H
#define ALEXA_H

#include <QObject>
#include "bearertokenjob.h"

class AlexaAdaptor;
class MultipartHeaders;
class MultipartReader;
class QNetworkReply;
class QNetworkAccessManager;


class Alexa : public QObject
{
    Q_OBJECT
    Q_PROPERTY(AlexaStatus status READ status)
    Q_PROPERTY(bool authenticated READ authenticated )

public:
    enum AlexaStatus {
        StatusOffline,
        StatusAuthentication,
        StatusToken,
        StatusAsking,
        StatusSendQuestion,
        StatusGetAnswer,
        StatusIdle,
        StatusAuthenticationFailed,
        StatusTokenFailed,
        StatusSendQuestionFailed,
    };

    explicit Alexa(QObject *parent = 0);
    ~Alexa();

    QString productId() const;
    void setProductId(const QString &productId);

    AlexaStatus status() const;
    void setStatus( AlexaStatus status );

    bool authenticated() const;
    void setAuthenticated( bool auth );

signals:
    void error(int code, const QString &message);
    void userSigninPrompt( const QUrl &url);

private:
    void emitPropertyChanged(const QString &name, const QVariant &value);
    void doAsk();

public slots:
    void startAsking();
    void endAsking();
    void authenticate( const QString &clientId, const QString &clientSecret );
    void userRedirect( const QString &url);

private slots:
    void queryFinished( QNetworkReply *reply);

private:
    static void onPartBegin(const MultipartHeaders &headers, void *userData);
    static void onPartData(const char *buffer, size_t size, void *userData);
    static void onPartEnd(void *userData);
    static void onEnd(void *userData);

private:
    AlexaAdaptor *m_adaptor;
    QNetworkAccessManager *m_nam;
    MultipartReader *m_parser;

    QString m_clientId; //amzn1.application-oa2-client.9a999ab5daf045f5a0648fc51d673496
    QString m_clientSecret; // a995a4a08f3d5da8b06748b3b471f97b2f8abcb31eae8e90c9f6511b4b664417
    QString m_productId; // rockstar_qml
    AlexaStatus m_status;
    bool m_authenticated;
    AccessToken m_token;
    QString m_refreshToken;

    // auth request job https://www.amazon.com/ap/oa
    // get token job https://api.amazon.com/auth/o2/token
    // refresh token job https://api.amazon.com/auth/o2/token
    // ask question job https://access-alexa-na.amazon.com/v1/avs/speechrecognizer/recognize
};

#endif // ALEXA_H

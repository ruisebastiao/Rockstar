#ifndef AUTHENTICATEREQUESTJOB_H
#define AUTHENTICATEREQUESTJOB_H

#include <QObject>

class QNetworkReply;
class QNetworkAccessManager;
class AuthenticateRequestJob : public QObject
{
    Q_OBJECT
public:
    explicit AuthenticateRequestJob(QNetworkAccessManager *parent);

    void authenticate( const QString &productID, const QString &clientID, const QString &clientSecret );
    void extractCode(const QUrl &redirect, const QString &clientID, const QString &clientSecret);

signals:
    void userPrompt( const QUrl &redirect );
    void authToken( const QString &token );
    void finished();

private slots:
    void extractRedirect();
    void extractRefreshToken();

private:
    QNetworkAccessManager *m_nam;
    QString m_productId;
    QString m_clientId;
    QString m_clientSecret;
};

#endif // AUTHENTICATEREQUESTJOB_H

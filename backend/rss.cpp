#include "rss.h"
#include "rssarticle.h"
#include "dbusutilities.h"
#include "vntrssreader.h"
#include "vntrsschannel.h"
#include "vntrssitem.h"

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QTimer>

Rss::Rss(QObject *parent) : QObject(parent)
{
    m_adaptor = new RssAdaptor(this);

    QDBusConnection::sessionBus().registerObject("/pub/geekcentral/Rockstar/Rss", this);


    m_reader = new VNTRSSReader(this);
    connect( m_reader, SIGNAL(loadedRSS(QList<VNTRSSChannel*>)),
             this, SLOT(loadedRSS(QList<VNTRSSChannel*>)));

    QTimer *timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()), this, SLOT(refreshRss()));
    timer->setInterval(1 * 60 * 1000);
    timer->start();
}

QString Rss::feed() const
{
    return m_feed;
}

void Rss::setFeed(const QString &feed)
{
    if ( m_feed != feed ) {
        m_feed = feed;
        emitPropertyChanged("feed",m_feed);
        QMetaObject::invokeMethod(this, "refreshRss");
    }
}

QList<QDBusObjectPath> Rss::articles() const
{
    return m_articleLinks;
}

void Rss::refreshRss()
{
    if ( !m_feed.isEmpty() ) {
        m_reader->load(QUrl(m_feed));
        qDebug() << Q_FUNC_INFO << m_feed;
    }
}

void Rss::loadedRSS(QList<VNTRSSChannel *> rssChannels)
{
    qDebug() << Q_FUNC_INFO << rssChannels.count();

    // rebuild article list, populate links and emit it changed
    foreach( VNTRSSChannel *channel, rssChannels ) {
        foreach( VNTRSSItem *item, channel->getRSSItems()) {
            bool found = false;
            foreach(RssArticle *article, m_articles) {
                found = found || article->isEquivelent(item);
            }
            if ( !found ) {
                RssArticle *newArticle = new RssArticle(m_articles.count(),item, this);
                m_articleLinks << newArticle->path();
                m_articles << newArticle;
            }
        }
    }

    emitPropertyChanged("articles",QVariant::fromValue(m_articleLinks));
}

void Rss::emitPropertyChanged(const QString &name, const QVariant &value)
{
    DBusUtilities::emitPropertyChanged(QLatin1String("pub.geekcentral.Rockstar.Rss"),
                                       QLatin1String("/pub/geekcentral/Rockstar/Rss"),
                                       name,
                                       value);
}

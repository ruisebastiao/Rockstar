#include "rssarticle.h"
#include "dbusutilities.h"

RssArticle::RssArticle(int id, VNTRSSItem *item, QObject *parent) : QObject(parent), m_id(id)
{

    m_adaptor = new RssArticleAdaptor(this);

    QDBusConnection::sessionBus().registerObject(
                QString(QLatin1String("/pub/geekcentral/Rockstar/Rss/%1")).arg(m_id),
                this);

    m_title = item->getPlainTitle();
    m_description = item->getPlainDescription();
    m_image = item->getImageUrl();
    m_link = item->getLink();
    m_pubdate = item->getDate();
}

int RssArticle::id() const
{
    return m_id;
}

QString RssArticle::title() const
{
    return m_title;
}

void RssArticle::setTitle(const QString &title)
{
    if ( m_title != title ) {
        m_title = title;
        emitPropertyChanged("title", m_title);
    }
}

QString RssArticle::description() const
{
    return m_description;
}

void RssArticle::setDescription(const QString &description)
{
    if ( m_description != description ) {
        m_description = description;
        emitPropertyChanged("description", m_description);
    }
}

QUrl RssArticle::image() const
{
    return m_image;
}

void RssArticle::setImage(const QUrl &image)
{
    if ( m_image != image ) {
        m_image = image;
        emitPropertyChanged("image", m_image);
    }
}

QUrl RssArticle::link() const
{
    return m_link;
}

void RssArticle::setLink(const QUrl &link)
{
    if ( m_link != link ) {
        m_link = link;
        emitPropertyChanged("link", m_link);
    }
}

QDateTime RssArticle::pubdate() const
{
    return m_pubdate;
}

void RssArticle::setPubdate(const QDateTime &pubdate)
{
    if ( m_pubdate != pubdate ) {
        m_pubdate = pubdate;
        emitPropertyChanged("pubdate", m_pubdate);
    }
}

bool RssArticle::isEquivelent(const VNTRSSItem *article) const
{
    return (m_pubdate == article->getDate() ) &&
            (m_title == article->getTitle() );
}

QDBusObjectPath RssArticle::path() const
{
    return QDBusObjectPath(QString(QLatin1String("/pub/geekcentral/Rockstar/Rss/%1")).arg(m_id));
}

void RssArticle::emitPropertyChanged(const QString &name, const QVariant &value)
{

    DBusUtilities::emitPropertyChanged(QLatin1String("pub.geekcentral.Rockstar.RssArticle"),
                                       QString(QLatin1String("/pub/geekcentral/Rockstar/Rss/%1")).arg(m_id),
                                       name,
                                       value);
}
